#NiceHexSpiral.py
import turtle 
colors=['purple', 'blue','green','yellow','orange','red','white','navy blue','gray', 'pink']
#colors=['green', 'black', 'blue']
t=turtle.Pen()
turtle.bgcolor('black')
#turtle.bgcolor('white')
for x in range(360):
    t.pencolor(colors[x%10])
    t.width(x/100+1)
    t.forward(x)
    t.left(59)